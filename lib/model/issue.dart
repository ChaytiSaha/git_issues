class IssueApi {
  String _url;
  String _repositoryUrl;
  String _labelsUrl;
  String _commentsUrl;
  String _eventsUrl;
  String _htmlUrl;
  int _id;
  String _nodeId;
  int _number;
  String _title;
  User _user;
  String _state;
  bool _locked;
  Null _assignee;
  Null _milestone;
  int _comments;
  String _createdAt;
  String _updatedAt;
  String _closedAt;
  String _authorAssociation;
  String _activeLockReason;
  PullRequest _pullRequest;
  String _body;
  User _closedBy;
  String _timelineUrl;
  Null _performedViaGithubApp;

  IssueApi(
      {String url,
        String repositoryUrl,
        String labelsUrl,
        String commentsUrl,
        String eventsUrl,
        String htmlUrl,
        int id,
        String nodeId,
        int number,
        String title,
        User user,
        String state,
        bool locked,
        Null assignee,
        Null milestone,
        int comments,
        String createdAt,
        String updatedAt,
        String closedAt,
        String authorAssociation,
        String activeLockReason,
        PullRequest pullRequest,
        String body,
        User closedBy,
        String timelineUrl,
        Null performedViaGithubApp}) {
    this._url = url;
    this._repositoryUrl = repositoryUrl;
    this._labelsUrl = labelsUrl;
    this._commentsUrl = commentsUrl;
    this._eventsUrl = eventsUrl;
    this._htmlUrl = htmlUrl;
    this._id = id;
    this._nodeId = nodeId;
    this._number = number;
    this._title = title;
    this._user = user;
    this._state = state;
    this._locked = locked;
    this._assignee = assignee;
    this._milestone = milestone;
    this._comments = comments;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._closedAt = closedAt;
    this._authorAssociation = authorAssociation;
    this._activeLockReason = activeLockReason;
    this._pullRequest = pullRequest;
    this._body = body;
    this._closedBy = closedBy;
    this._timelineUrl = timelineUrl;
    this._performedViaGithubApp = performedViaGithubApp;
  }

  String get url => _url;
  set url(String url) => _url = url;
  String get repositoryUrl => _repositoryUrl;
  set repositoryUrl(String repositoryUrl) => _repositoryUrl = repositoryUrl;
  String get labelsUrl => _labelsUrl;
  set labelsUrl(String labelsUrl) => _labelsUrl = labelsUrl;
  String get commentsUrl => _commentsUrl;
  set commentsUrl(String commentsUrl) => _commentsUrl = commentsUrl;
  String get eventsUrl => _eventsUrl;
  set eventsUrl(String eventsUrl) => _eventsUrl = eventsUrl;
  String get htmlUrl => _htmlUrl;
  set htmlUrl(String htmlUrl) => _htmlUrl = htmlUrl;
  int get id => _id;
  set id(int id) => _id = id;
  String get nodeId => _nodeId;
  set nodeId(String nodeId) => _nodeId = nodeId;
  int get number => _number;
  set number(int number) => _number = number;
  String get title => _title;
  set title(String title) => _title = title;
  User get user => _user;
  set user(User user) => _user = user;
  String get state => _state;
  set state(String state) => _state = state;
  bool get locked => _locked;
  set locked(bool locked) => _locked = locked;
  Null get assignee => _assignee;
  set assignee(Null assignee) => _assignee = assignee;
  Null get milestone => _milestone;
  set milestone(Null milestone) => _milestone = milestone;
  int get comments => _comments;
  set comments(int comments) => _comments = comments;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  String get closedAt => _closedAt;
  set closedAt(String closedAt) => _closedAt = closedAt;
  String get authorAssociation => _authorAssociation;
  set authorAssociation(String authorAssociation) =>
      _authorAssociation = authorAssociation;
  String get activeLockReason => _activeLockReason;
  set activeLockReason(String activeLockReason) =>
      _activeLockReason = activeLockReason;
  PullRequest get pullRequest => _pullRequest;
  set pullRequest(PullRequest pullRequest) => _pullRequest = pullRequest;
  String get body => _body;
  set body(String body) => _body = body;
  User get closedBy => _closedBy;
  set closedBy(User closedBy) => _closedBy = closedBy;
  String get timelineUrl => _timelineUrl;
  set timelineUrl(String timelineUrl) => _timelineUrl = timelineUrl;
  Null get performedViaGithubApp => _performedViaGithubApp;
  set performedViaGithubApp(Null performedViaGithubApp) =>
      _performedViaGithubApp = performedViaGithubApp;

  IssueApi.fromJson(Map<String, dynamic> json) {
    _url = json['url'];
    _repositoryUrl = json['repository_url'];
    _labelsUrl = json['labels_url'];
    _commentsUrl = json['comments_url'];
    _eventsUrl = json['events_url'];
    _htmlUrl = json['html_url'];
    _id = json['id'];
    _nodeId = json['node_id'];
    _number = json['number'];
    _title = json['title'];
    _user = json['user'] != null ? new User.fromJson(json['user']) : null;
    _state = json['state'];
    _locked = json['locked'];
    _assignee = json['assignee'];
    _milestone = json['milestone'];
    _comments = json['comments'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _closedAt = json['closed_at'];
    _authorAssociation = json['author_association'];
    _activeLockReason = json['active_lock_reason'];
    _pullRequest = json['pull_request'] != null
        ? new PullRequest.fromJson(json['pull_request'])
        : null;
    _body = json['body'];
    _closedBy =
    json['closed_by'] != null ? new User.fromJson(json['closed_by']) : null;
    _timelineUrl = json['timeline_url'];
    _performedViaGithubApp = json['performed_via_github_app'];
  }
}

class User {
  String _login;
  int _id;
  String _nodeId;
  String _avatarUrl;
  String _gravatarId;
  String _url;
  String _htmlUrl;
  String _followersUrl;
  String _followingUrl;
  String _gistsUrl;
  String _starredUrl;
  String _subscriptionsUrl;
  String _organizationsUrl;
  String _reposUrl;
  String _eventsUrl;
  String _receivedEventsUrl;
  String _type;
  bool _siteAdmin;

  User(
      {String login,
        int id,
        String nodeId,
        String avatarUrl,
        String gravatarId,
        String url,
        String htmlUrl,
        String followersUrl,
        String followingUrl,
        String gistsUrl,
        String starredUrl,
        String subscriptionsUrl,
        String organizationsUrl,
        String reposUrl,
        String eventsUrl,
        String receivedEventsUrl,
        String type,
        bool siteAdmin}) {
    this._login = login;
    this._id = id;
    this._nodeId = nodeId;
    this._avatarUrl = avatarUrl;
    this._gravatarId = gravatarId;
    this._url = url;
    this._htmlUrl = htmlUrl;
    this._followersUrl = followersUrl;
    this._followingUrl = followingUrl;
    this._gistsUrl = gistsUrl;
    this._starredUrl = starredUrl;
    this._subscriptionsUrl = subscriptionsUrl;
    this._organizationsUrl = organizationsUrl;
    this._reposUrl = reposUrl;
    this._eventsUrl = eventsUrl;
    this._receivedEventsUrl = receivedEventsUrl;
    this._type = type;
    this._siteAdmin = siteAdmin;
  }

  String get login => _login;
  set login(String login) => _login = login;
  int get id => _id;
  set id(int id) => _id = id;
  String get nodeId => _nodeId;
  set nodeId(String nodeId) => _nodeId = nodeId;
  String get avatarUrl => _avatarUrl;
  set avatarUrl(String avatarUrl) => _avatarUrl = avatarUrl;
  String get gravatarId => _gravatarId;
  set gravatarId(String gravatarId) => _gravatarId = gravatarId;
  String get url => _url;
  set url(String url) => _url = url;
  String get htmlUrl => _htmlUrl;
  set htmlUrl(String htmlUrl) => _htmlUrl = htmlUrl;
  String get followersUrl => _followersUrl;
  set followersUrl(String followersUrl) => _followersUrl = followersUrl;
  String get followingUrl => _followingUrl;
  set followingUrl(String followingUrl) => _followingUrl = followingUrl;
  String get gistsUrl => _gistsUrl;
  set gistsUrl(String gistsUrl) => _gistsUrl = gistsUrl;
  String get starredUrl => _starredUrl;
  set starredUrl(String starredUrl) => _starredUrl = starredUrl;
  String get subscriptionsUrl => _subscriptionsUrl;
  set subscriptionsUrl(String subscriptionsUrl) =>
      _subscriptionsUrl = subscriptionsUrl;
  String get organizationsUrl => _organizationsUrl;
  set organizationsUrl(String organizationsUrl) =>
      _organizationsUrl = organizationsUrl;
  String get reposUrl => _reposUrl;
  set reposUrl(String reposUrl) => _reposUrl = reposUrl;
  String get eventsUrl => _eventsUrl;
  set eventsUrl(String eventsUrl) => _eventsUrl = eventsUrl;
  String get receivedEventsUrl => _receivedEventsUrl;
  set receivedEventsUrl(String receivedEventsUrl) =>
      _receivedEventsUrl = receivedEventsUrl;
  String get type => _type;
  set type(String type) => _type = type;
  bool get siteAdmin => _siteAdmin;
  set siteAdmin(bool siteAdmin) => _siteAdmin = siteAdmin;

  User.fromJson(Map<String, dynamic> json) {
    _login = json['login'];
    _id = json['id'];
    _nodeId = json['node_id'];
    _avatarUrl = json['avatar_url'];
    _gravatarId = json['gravatar_id'];
    _url = json['url'];
    _htmlUrl = json['html_url'];
    _followersUrl = json['followers_url'];
    _followingUrl = json['following_url'];
    _gistsUrl = json['gists_url'];
    _starredUrl = json['starred_url'];
    _subscriptionsUrl = json['subscriptions_url'];
    _organizationsUrl = json['organizations_url'];
    _reposUrl = json['repos_url'];
    _eventsUrl = json['events_url'];
    _receivedEventsUrl = json['received_events_url'];
    _type = json['type'];
    _siteAdmin = json['site_admin'];
  }
}

class PullRequest {
  String _url;
  String _htmlUrl;
  String _diffUrl;
  String _patchUrl;

  PullRequest({String url, String htmlUrl, String diffUrl, String patchUrl}) {
    this._url = url;
    this._htmlUrl = htmlUrl;
    this._diffUrl = diffUrl;
    this._patchUrl = patchUrl;
  }

  String get url => _url;
  set url(String url) => _url = url;
  String get htmlUrl => _htmlUrl;
  set htmlUrl(String htmlUrl) => _htmlUrl = htmlUrl;
  String get diffUrl => _diffUrl;
  set diffUrl(String diffUrl) => _diffUrl = diffUrl;
  String get patchUrl => _patchUrl;
  set patchUrl(String patchUrl) => _patchUrl = patchUrl;

  PullRequest.fromJson(Map<String, dynamic> json) {
    _url = json['url'];
    _htmlUrl = json['html_url'];
    _diffUrl = json['diff_url'];
    _patchUrl = json['patch_url'];
  }

}
