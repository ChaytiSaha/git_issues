import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomMaterialButton extends StatelessWidget {

  final issueType;
  final onPressed;

  CustomMaterialButton({
    this.issueType,
    this.onPressed
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
          child: MaterialButton(
            onPressed: onPressed,
            color: Colors.blueGrey,
            textColor: Colors.white,
            elevation: 0.2,
            child: Row(
              children: [
                Expanded(
                  child: issueType,
                ),
                Icon(Icons.arrow_drop_down)
              ],
            ),
          )
      );
  }
}