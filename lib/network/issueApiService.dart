import 'package:dio/dio.dart';
import 'package:git_issues/constant/api.dart';
import 'package:git_issues/model/issue.dart';

class IssueApiService {

  String _baseUrl = ApiConstant.BASE_URL;
  Dio _dio = new Dio();

  Future fetchIssue(id) async{
    try{
      Response response = await _dio.get("$_baseUrl/$id");
      IssueApi issueApiResponse = IssueApi.fromJson(response.data);
      return issueApiResponse;
    } on DioError catch(e){
      print(e);
    }
  }

}