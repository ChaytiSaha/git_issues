import 'package:flutter/material.dart';
import 'package:git_issues/mode/darkThemeData.dart';
import 'package:git_issues/mode/darkThemeProvider.dart';
import 'package:git_issues/network/issueApiService.dart';
import 'package:git_issues/provider/issueListProvider.dart';
import 'package:git_issues/provider/issueProvider.dart';
import 'package:provider/provider.dart';
import 'view/homePage.dart';

void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create:(context) => IssuesProvider(issueApiService: IssueApiService())),
    ChangeNotifierProvider(create: (context) => IssueListProvider()),
  ], child: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool switchValue = false;
  DarkThemeProvider _darkThemeProvider = DarkThemeProvider();

  void getCurrentTheme() async {
    _darkThemeProvider.darkTheme =
    await _darkThemeProvider.darkThemePreference.getTheme();
  }

  @override
  void initState() {
    getCurrentTheme();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _darkThemeProvider,
      child: Consumer<DarkThemeProvider>(
        builder: (context, value, child) {
          return MaterialApp(
              debugShowCheckedModeBanner: false,
              theme: Styles.themeData(_darkThemeProvider.darkTheme, context),
              darkTheme: Styles.themeData(true, context),
              home: Scaffold(
                  appBar: AppBar(
                    title: Text('Git Issues'),
                    leading: IconButton(
                      icon: Icon(Icons.menu_outlined),
                      onPressed: () {},
                    ),
                    actions: [
                      Switch(
                          value: switchValue,
                          onChanged: (val) {
                            _darkThemeProvider.darkTheme =
                            !_darkThemeProvider.darkTheme;
                            setState(() {
                              switchValue = val;
                            });
                          })
                    ],
                  ),
                  body: HomePage()));
        },
      ),
    );
  }
}
