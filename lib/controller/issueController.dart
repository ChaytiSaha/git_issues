import 'package:git_issues/model/issue.dart';
import 'package:git_issues/network/issueApiService.dart';

class IssueController {
  Future<IssueApi> getIssueById(int id) async {
    var response = await IssueApiService().fetchIssue(id);
    return response;
  }
}