import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:git_issues/constant/api.dart';
import 'package:http/http.dart';

class IssueListProvider extends ChangeNotifier {

  final _baseUrl = ApiConstant.BASE_URL;
  List _issues = [], _fetchedIssues = [];
  int _perPage = 10, index = 0, hasNextPage = 1;

  //first load & after clicking search btn load
  void initIssueList(String selectedFilterType, String selectedSortType, int _page) async {

    final response = await Client().get(Uri.parse("$_baseUrl?page=$_page&per_page=$_perPage&filter=$selectedFilterType&sort=$selectedSortType"));
    try{
      _issues = await json.decode((response.body));
    }catch(e){
      print(e.message);
      throw Exception(e.message);
    }
    notifyListeners();

  }


  void totIssue(){
    _issues=[];
    notifyListeners();
  }


  List get issues => _issues;

  set issues(List value) {
    _issues = value;
    notifyListeners();
  }




  //load more
  void moreIssueList(String selectedFilterType, String selectedSortType, int _page) async {

    final response = await Client().get(Uri.parse("$_baseUrl?page=$_page&per_page=$_perPage&filter=$selectedFilterType&sort=$selectedSortType"));
    try{
       _fetchedIssues = await json.decode((response.body));
      _issues.addAll(_fetchedIssues);
    }catch(e){
      print(e.message);
      throw Exception(e.message);
    }
    if(_fetchedIssues.length == 0) hasNextPage = 0;
    notifyListeners();

  }

  List get fetchedIssues => _fetchedIssues;

  set fetchedIssues(List value) {
    _fetchedIssues = value;
    notifyListeners();
  }

  void totFetchedIssue(){
    _fetchedIssues=[];
    index++;
    notifyListeners();
  }

}
