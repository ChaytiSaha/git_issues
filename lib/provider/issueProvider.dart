import 'package:flutter/cupertino.dart';
import 'package:git_issues/model/issue.dart';
import 'package:git_issues/network/issueApiService.dart';

class IssuesProvider extends ChangeNotifier {
  final IssueApiService issueApiService;

  IssuesProvider({@required this.issueApiService});

  IssueApi _issues;
  int _issueIndex;

  void initIssue(int id) async {
    // if (_issues==null) {
      try{
        _issues = await issueApiService.fetchIssue(id);
      }catch(e){
        print(e.message);
      }
      _issueIndex = 0;
      notifyListeners();
    // }
  }

  IssueApi get issues => _issues;

  set issues(IssueApi value) {
    _issues = value;
    notifyListeners();
  }

  int get issueIndex => _issueIndex;

  set issueIndex(int value) {
    _issueIndex = value;
    notifyListeners();
  }
}
