import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:git_issues/mode/darkThemeData.dart';
import 'package:git_issues/mode/darkThemeProvider.dart';
import 'package:git_issues/provider/issueProvider.dart';
import 'package:provider/provider.dart';

class IssueDetails extends StatefulWidget {

  //getting id of issue to get details
  final id;
  IssueDetails(this.id);

  @override
  _IssueDetailsState createState() => _IssueDetailsState();
}

class _IssueDetailsState extends State<IssueDetails> {

  DarkThemeProvider _darkThemeProvider = DarkThemeProvider();
  void getCurrentTheme() async{
    _darkThemeProvider.darkTheme = await _darkThemeProvider.darkThemePreference.getTheme();
  }

  void _loadIssue(int id) {
    Provider.of<IssuesProvider>(context, listen: false).initIssue(id);
  }

  @override
  void initState() {
    getCurrentTheme();
    super.initState();
    _loadIssue(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    final providerIssue = Provider.of<IssuesProvider>(context);
    return ChangeNotifierProvider(
      create: (_) => _darkThemeProvider,
      child: Consumer<DarkThemeProvider>(
        builder: (context,value,child){
          return (providerIssue.issues != null && providerIssue.issues.number == widget.id) ? MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: Styles.themeData(_darkThemeProvider.darkTheme, context),
            darkTheme: Styles.themeData(true, context),
            home: Scaffold(
                appBar: AppBar(
                  title: Text('Issue Details'),
                  leading: IconButton(icon: Icon(Icons.menu_outlined),onPressed: (){},),
                ),
                body: SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        Container(
                          height: 55,
                          child: Text(providerIssue.issues.title,
                            overflow: TextOverflow.clip,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),),
                        ),
                        Text(providerIssue.issues.id.toString()),
                        Text("Current Status : ${providerIssue.issues.state.toUpperCase()}"),

                        SizedBox(height: 20.0),
                        Text("User", style: TextStyle(fontWeight: FontWeight.bold),),
                        if(providerIssue.issues?.user?.avatarUrl != null)
                          CircleAvatar(radius: 50.0,backgroundImage: NetworkImage(providerIssue.issues.user.avatarUrl)),
                        Text(providerIssue.issues?.user?.login??''),

                        SizedBox(child: Divider(height: 30.0,color: Colors.blueGrey,),),

                        Row(children: [Text("Assigned by : abc")]),
                        Row(
                          children: [
                            Text("Created at    : "),
                            if(providerIssue.issues.createdAt != null) Text(providerIssue.issues.createdAt.substring(0,10)),
                            if(providerIssue.issues.createdAt != null) Text(" at " + providerIssue.issues.createdAt.substring(12,19)),
                          ],
                        ),
                        Row(
                          children: [
                            Text("Updated at   : "),
                            if(providerIssue.issues.updatedAt != null) Text(providerIssue.issues.updatedAt.substring(0,10)),
                            if(providerIssue.issues.updatedAt != null) Text(" at " + providerIssue.issues.updatedAt.substring(12,19)),
                          ],
                        ),

                        Row(children: [Text("Closed by     :  ${providerIssue.issues?.closedBy?.login??''}")]),
                        Row(
                          children: [
                            Text("Closed at      : "),
                            if(providerIssue.issues.closedAt != null) Text(providerIssue.issues.closedAt.substring(0,10)),
                            if(providerIssue.issues.closedAt != null) Text(" at " + providerIssue.issues.closedAt.substring(12,19)),
                          ],
                        ),

                        SizedBox(child: Divider(height: 30.0,color: Colors.blueGrey,),),

                        Text("Description",
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0
                          ),),
                        Container(
                          height: 300.0,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            physics: ClampingScrollPhysics(),
                            child: Text(providerIssue.issues?.body??""),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
            ),
          ):Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
