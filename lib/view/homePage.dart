import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:git_issues/provider/issueListProvider.dart';
import 'package:git_issues/utils/customButton.dart';
import 'package:git_issues/view/issueDetailsPage.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  int _page = 1;
  ScrollController _controller;

  var filterTypeList = ['assigned', 'created', 'mentioned', 'subscribed', 'all'];
  String selectedFilterType = 'all';

  var sortTypeList = ['created', 'updated', 'comments'];
  String selectedSortType = 'created';

  //first time loading api
  void _firstLoad() async{
    final providerIssueList = Provider.of<IssueListProvider>(context,listen: false);
    providerIssueList.initIssueList(selectedFilterType, selectedSortType, _page);
  }

  //api loading all time after first time
  void _loadMore() async{
    if(_controller.position.pixels == _controller.position.maxScrollExtent){
      _page += 1;
      final providerIssueList = Provider.of<IssueListProvider>(context,listen: false);
      providerIssueList.moreIssueList(selectedFilterType, selectedSortType, _page);
      providerIssueList.totFetchedIssue();
    }
  }

  @override
  void initState() {
    super.initState();
    _firstLoad();
    _controller = new ScrollController()..addListener(_loadMore);
  }

  @override
  void dispose() {
    _controller.removeListener(_loadMore);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final providerIssueList = Provider.of<IssueListProvider>(context);
    return SafeArea(
        child: providerIssueList.issues.length==0 ? Center(
          child: CircularProgressIndicator(), //loading
        ):Column(
          children: [
            Row( //filter & sort type given by user
              children: [
                CustomMaterialButton(
                  issueType: Text("Filter by: $selectedFilterType"),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                              backgroundColor: Colors.blueGrey,
                              insetPadding: EdgeInsets.symmetric(horizontal: 120),
                              actions: [
                                ButtonTheme(
                                  child: DropdownButton(
                                    underline: Container(
                                      height: 0.5,
                                      color: Colors.white,
                                    ),
                                    hint: Text('Select Type',style: TextStyle(color: Colors.white)),
                                    isExpanded: false,
                                    onChanged: (value) {
                                      setState(() {
                                        selectedFilterType = value;
                                        Navigator.of(context).pop(context);
                                      });
                                    },
                                    items: filterTypeList.map<DropdownMenuItem<dynamic>>((value) {
                                      return DropdownMenuItem(
                                          value: value,
                                          child: Text(value)
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ]);
                        });
                  },
                ),
                SizedBox(
                    width:10.0
                ),
                CustomMaterialButton(
                  issueType: Text("Sort by: $selectedSortType"),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                              backgroundColor: Colors.blueGrey,
                              insetPadding: EdgeInsets.symmetric(horizontal: 120),
                              actions: [
                                ButtonTheme(
                                  child: DropdownButton(
                                    underline: Container(
                                      height: 0.5,
                                      color: Colors.white,
                                    ),
                                    hint: Text('Select Type',style: TextStyle(color: Colors.white)),
                                    isExpanded: false,
                                    onChanged: (value) {
                                      setState(() {
                                        selectedSortType = value;
                                        Navigator.of(context).pop(context);
                                      });
                                    },
                                    items: sortTypeList.map<DropdownMenuItem<dynamic>>((value) {
                                      return DropdownMenuItem(
                                          value: value,
                                          child: Text(value)
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ]);
                        });
                  },
                )
              ],
            ),
            Center( //search button
                child:RaisedButton(
                  child: Text('Search'),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)
                  ),
                  onPressed: (){
                    _page = 1;
                    providerIssueList.totIssue();
                    _firstLoad();
                  },
                )
            ),
            Expanded(//all issues
                child: ListView.builder(
                  controller: _controller,
                  itemCount: providerIssueList.issues.length,
                  itemBuilder: (_,index) => Card(
                    margin: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
                    child: InkWell(
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => IssueDetails(providerIssueList.issues[index]["number"])));
                      },
                      child: ListTile(
                        title: Text(providerIssueList.issues[index]["title"]),
                        subtitle: Text(providerIssueList.issues[index]["id"].toString()),
                      ),
                    ),
                  ),
                )
            ),

            if(providerIssueList.fetchedIssues.length == 0 && providerIssueList.index > 0)
              Padding(
                padding: const EdgeInsets.only(top:10, bottom:40),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),

            if(providerIssueList.hasNextPage == 0)
              Container(
                padding: const EdgeInsets.only(top:30, bottom: 40),
                color: Colors.amber,
                child: Center(
                  child: Text('All of the issues have been fetched'),
                ),
              )
          ],
        )
    );
  }
}
