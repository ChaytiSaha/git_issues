# git_issues

Task completed for the coding test.

[App demonstration](https://drive.google.com/file/d/1KQOpoGOmYUJuAVLGkQxWnA9o5nW3e9JT/view?usp=sharing)

##Functionalities:
- Fetching git issues api & display all issues
- [Api details](https://docs.github.com/en/rest/reference/issues)
- [issues api](https://api.github.com/repos/flutter/flutter/issues)
- Navigate to issue details page for all issues
- Switch to dark & light mode (this mode is also applicable dependingon whole phone's mode. if phone mode is dark, then     this add will stay in dark mode)
- Filter & Sort the issues 

##Technical details:
- architecture: MVC 
- api call: http, dio
- state management: provider
- theme/ mode status storage: shared preference
- pagination - per page:10 issues

##Packages:
- cupertino_icons: ^1.0.2
- dio: ^4.0.0
- http: ^0.13.3
- shared_preferences: ^2.0.6
- provider: ^6.0.0

